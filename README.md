**Energy Rocks MCo**

We welcome you to open your heart and mind and take a challenge to see how Nature’s Art can be a part of your everyday surroundings to define beauty from the INSIDE / OUT. An individualized approach for introducing energy rocks and crystal grid layouts of pieces, each tailored to the individual and property. Our consulting program will educate and infuse energy in homes and commercial establishments through the use of energy stones.

*[https://michalandcompany.com/energy-rocks-consulting](https://michalandcompany.com/energy-rocks-consulting/)
---
